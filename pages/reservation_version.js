import React from 'react';
import Layout from "../components/Layout";
import Header from "../components/Header";
import Footer from "../components/Footer";


const reservation_version = () => {

return (
<Layout pageTitle="Tulevik ">
<Header/>

<main>
		<div id="form_container">
			<div className="row">
				<div className="col-lg-5">
					<div id="left_form">
						<figure><img src="/assets/img/reserve_bg.svg" alt=""/></figure>
						<h2>Reservation</h2>
						<p>Tation argumentum et usu, dicit viderer evertitur te has. Eu dictas concludaturque usu, facete detracto patrioque an per, lucilius pertinacia eu vel.</p>
						<a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i className="pe-7s-info"></i></a>
					</div>
				</div>
				<div className="col-lg-7">

					<div id="wizard_container">
						<div id="top-wizard">
							<div id="progressbar"></div>
						</div>
					
						<form name="example-1" id="wrapped" method="POST">
							<input id="website" name="website" type="text" value=""/>
							
							<div id="middle-wizard">
								
								<div className="step">
									<h3 className="main_question"><strong>1/3</strong>Please fill with your request</h3>
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input className="form-control required" type="text" name="check_in" id="check_in" placeholder="Check in"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input className="form-control required" type="text" name="check_out" id="check_out" placeholder="Check out"/>
											</div>
										</div>
									</div>
									
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<div className="qty-buttons">
													<input type="button" value="+" className="qtyplus" name="adults"/>
													<input type="text" name="adults" value="" className="qty form-control required" placeholder="Adults"/>
													<input type="button" value="-" className="qtyminus" name="adults"/>
												</div>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<div className="qty-buttons">
													<input type="button" value="+" className="qtyplus" name="childs"/>
													<input type="text" name="childs" value="" className="qty form-control" placeholder="Child"/>
													<input type="button" value="-" className="qtyminus" name="childs"/>
												</div>
											</div>
										</div>
									</div>
									
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<div className="styled-select">
													<select className="required" name="room_type">
														<option value="" selected>Select room type</option>
														<option value="Single bed">Single bed</option>
														<option value="Double Bed">Double Bed</option>
														<option value="Luxury double bed">Luxury double bed</option>
														<option value="Suite">Suite</option>
													</select>
												</div>
											</div>
										</div>
									</div>
									
								</div>

								<div className="step">
									<h3 className="main_question"><strong>2/3</strong>Please fill with your details</h3>
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="firstname" className="form-control required" placeholder="First name"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="lastname" className="form-control required" placeholder="Last name"/>
											</div>
										</div>
									</div>
									
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="email" name="email" className="form-control required" placeholder="Your Email"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="telephone" className="form-control" placeholder="Your Telephone"/>
											</div>
										</div>
									</div>
									
								</div>
								
								<div className="submit step">
									<h3 className="main_question"><strong>3/3</strong>Send an optional message</h3>
									<div className="form-group">
										<textarea name="additional_message" className="form-control" style={{height:"150px"}} placeholder="Hello world....write your messagere here!"></textarea>
									</div>
									<div className="form-group terms">
										<input name="terms" type="checkbox" className="icheck required" value="yes"/>
										<label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</label>
									</div>
								</div>
								
							</div>
							
							<div id="bottom-wizard">
								<button type="button" name="backward" className="backward">Backward </button>
								<button type="button" name="forward" className="forward">Forward</button>
								<button type="submit" name="process" className="submit">Submit</button>
							</div>
							
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</main>

<Footer/>
</Layout>
	)




}
export default  reservation_version;