import React from 'react';
import Layout from "../components/Layout";
import Header from "../components/Header";
import Footer from "../components/Footer";


const Questionare = () => {

return (
<Layout pageTitle="Tulevik ">
<Header/>
<main>
		<div id="form_container">
			<div className="row">
				<div className="col-lg-5">
					<div id="left_form">
						<figure><img src="/assets/img/registration_bg.svg" alt=""/></figure>
						<h2>Questionaire</h2>
						<p>Tation argumentum et usu, dicit viderer evertitur te has. Eu dictas concludaturque usu, facete detracto patrioque an per, lucilius pertinacia eu vel.</p>
						<a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i className="pe-7s-info"></i></a>
					</div>
				</div>
				<div className="col-lg-7">

					<div id="wizard_container">
						<div id="top-wizard">
							<div id="progressbar"></div>
						</div>
						{/* <!-- /top-wizard --> */}
						<form name="example-1" id="wrapped" method="POST">
							<input id="website" name="website" type="text" value=""/>
							{/* <!-- Leave for security protection, read docs for details --> */}
							<div id="middle-wizard">
								
								<div className="step">
									<h3 className="main_question"><strong>1/3</strong>How do you describe your overall satisfaction?</h3>
									<div className="row">
										<div className="col-md-12">
											<div className="form-group radio_input">
												<label><input type="radio" value="Satisfied" name="satisfaction" className="icheck required"/>I am very satisfied</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="radio" value="Somewhat satisfied" name="satisfaction" className="icheck required"/>I am somewhat satisfied</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="radio" value="Neutral" name="satisfaction" className="icheck required"/>Neutral</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="radio" value="Somewhat dissatisfied" name="satisfaction" className="icheck required"/>I am somewhat dissatisfied</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="radio" value="Very dissatisfied" name="satisfaction" className="icheck required"/>I am very dissatisfied</label>
											</div>
										</div>
									</div>
									{/* <!-- /row --> */}
								</div>
								{/* <!-- /step --> */}
								
								<div className="step">
									<h3 className="main_question"><strong>2/3</strong>How do you describe your carrer or profile?</h3>
									<div className="row">
										<div className="col-md-6">
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Professional" name="question_2[]" className="icheck required"/>Professional</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Graduated" name="question_2[]" className="icheck required"/>Graduated</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Not graduated" name="question_2[]" className="icheck required"/>Not graduated</label>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Emphatic" name="question_2[]" className="icheck required"/>Emphatic</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Motivated" name="question_2[]" className="icheck required"/>Motivated</label>
											</div>
											<div className="form-group radio_input">
												<label><input type="checkbox" value="Passionate" name="question_2[]" className="icheck required"/>Passionate</label>
											</div>
										</div>
									</div>
									{/* <!-- /row --> */}
								</div>
								{/* <!-- /step --> */}

								<div className="submit step">
									<h3 className="main_question"><strong>3/3</strong>Please fill with your details</h3>
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="firstname" className="form-control required" placeholder="First name"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="lastname" className="form-control required" placeholder="Last name"/>
											</div>
										</div>
									</div>
									{/* <!-- /row --> */}

									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="email" name="email" className="form-control required" placeholder="Your Email"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="telephone" className="form-control" placeholder="Your Telephone"/>
											</div>
										</div>
									</div>
									{/* <!-- /row --> */}
									<br/>
									<div className="row">
										<div className="col-md-12">
											<div className="form-group terms">
												<input name="terms" type="checkbox" className="icheck required" value="yes"/>
												<label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</label>
											</div>
										</div>
									</div>
								</div>
								{/* <!-- /step--> */}
							</div>
							{/* <!-- /middle-wizard --> */}
							<div id="bottom-wizard">
								<button type="button" name="backward" className="backward">Backward </button>
								<button type="button" name="forward" className="forward">Forward</button>
								<button type="submit" name="process" className="submit">Submit</button>
							</div>
							{/* <!-- /bottom-wizard --> */}
						</form>
					</div>
					{/* <!-- /Wizard container --> */}
				</div>
			</div>
			{/* <!-- /Row --> */}
		</div>
		{/* <!-- /Form_container --> */}
	</main>

<Footer/>
</Layout>
	)




}
export default  Questionare;