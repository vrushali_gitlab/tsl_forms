
import Layout from "../components/Layout";
 import FirstPage from "../components/FirstPage";
import Header from "../components/Header";
import Footer from "../components/Footer";
// import Clients from "../components/Clients";
// import Services from "../components/Services";
// import About from "../components/About";
// import Products from "../components/Products";
// import Float from "../components/Float";
// import Topbar from "../components/Topbar";
// import Contact from "../components/Contact";


const HomePage = () => (

  <Layout pageTitle="Tulevik ">
    <Header></Header>
      <FirstPage />   
      <Footer /> 
  </Layout>

)

export default HomePage;