import React from 'react';
import Layout from "../components/Layout";
import Header from "../components/Header";
import Footer from "../components/Footer";


const review_version = () => {

return (
<Layout pageTitle="Tulevik ">
<Header/>

<main>
		<div id="form_container">
			<div className="row">
				<div className="col-lg-5">
					<div id="left_form">
						<figure><img src="/assets/img/review_bg.svg" alt=""/></figure>
						<h2>Reviews</h2>
						<p>Tation argumentum et usu, dicit viderer evertitur te has. Eu dictas concludaturque usu, facete detracto patrioque an per, lucilius pertinacia eu vel.</p>
						<a href="#0" id="more_info" data-toggle="modal" data-target="#more-info"><i className="pe-7s-info"></i></a>
					</div>
				</div>
				<div className="col-lg-7">

					<div id="wizard_container">
						<div id="top-wizard">
							<div id="progressbar"></div>
						</div>
						
						<form name="example-1" id="wrapped" method="POST">
							<input id="website" name="website" type="text" value=""/>
							
							<div id="middle-wizard">
								
								<div className="step">
									<h3 className="main_question"><strong>1/3</strong>How do you describe your overall satisfaction?</h3>
									<div className="row">
										<div className="col-md-12">
											<div className="form-group clearfix">
												<label className="rating_type">Service provided</label>
												<span className="rating">
												<input type="radio" className="required rating-input" id="rating-input-1-5" name="rating_input_1" value="5 Stars"/><label for="rating-input-1-5" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-1-4" name="rating_input_1" value="4 Stars"/><label for="rating-input-1-4" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-1-3" name="rating_input_1" value="3 Stars"/><label for="rating-input-1-3" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-1-2" name="rating_input_1" value="2 Stars"/><label for="rating-input-1-2" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-1-1" name="rating_input_1" value="1 Star"/><label for="rating-input-1-1" className="rating-star"></label>
												</span>
											</div>
											<div className="form-group clearfix">
												<label className="rating_type">Product's quality</label>
												<span className="rating">
												<input type="radio" className="required rating-input" id="rating-input-2-5" name="rating_input_2" value="5 Stars"/><label for="rating-input-2-5" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-2-4" name="rating_input_2" value="4 Stars"/><label for="rating-input-2-4" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-2-3" name="rating_input_2" value="3 Stars"/><label for="rating-input-2-3" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-2-2" name="rating_input_2" value="2 Stars"/><label for="rating-input-2-2" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-2-1" name="rating_input_2" value="1 Star"/><label for="rating-input-2-1" className="rating-star"></label>
												</span>
											</div>
											<div className="form-group clearfix">
												<label className="rating_type">Support</label>
												<span className="rating">
												<input type="radio" className="required rating-input" id="rating-input-3-5" name="rating_input_3" value="5 Stars"/><label for="rating-input-3-5" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-3-4" name="rating_input_3" value="4 Stars"/><label for="rating-input-3-4" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-3-3" name="rating_input_3" value="3 Stars"/><label for="rating-input-3-3" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-3-2" name="rating_input_3" value="2 Stars"/><label for="rating-input-3-2" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-3-1" name="rating_input_3" value="1 Star"/><label for="rating-input-3-1" className="rating-star"></label>
												</span>
											</div>
											<div className="form-group clearfix">
												<label className="rating_type">General satisfaction</label>
												<span className="rating">
												<input type="radio" className="required rating-input" id="rating-input-4-5" name="rating_input_4" value="5 Stars"/><label for="rating-input-4-5" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-4-4" name="rating_input_4" value="4 Stars"/><label for="rating-input-4-4" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-4-3" name="rating_input_4" value="3 Stars"/><label for="rating-input-4-3" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-4-2" name="rating_input_4" value="2 Stars"/><label for="rating-input-4-2" className="rating-star"></label>
												<input type="radio" className="required rating-input" id="rating-input-4-1" name="rating_input_4" value="1 Star"/><label for="rating-input-4-1" className="rating-star"></label>
												</span>
											</div>
										</div>
									</div>
									
								</div>
							
								
								<div className="step">
									<h3 className="main_question"><strong>2/3</strong>Your review and comments</h3>
									<div className="row">
										<div className="col-md-12">
											<div className="form-group">
												<textarea name="review" className="form-control required" style={{height:"180px"}} placeholder="Hello world....write your review here!"></textarea>
											</div>
										</div>
									</div>
								
								</div>
							
								<div className="submit step">
									<h3 className="main_question"><strong>3/3</strong>Please fill with your details</h3>
									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="firstname" className="form-control required" placeholder="First name"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="lastname" className="form-control required" placeholder="Last name"/>
											</div>
										</div>
									</div>
									

									<div className="row">
										<div className="col-md-6">
											<div className="form-group">
												<input type="email" name="email" className="form-control required" placeholder="Your Email"/>
											</div>
										</div>
										<div className="col-md-6">
											<div className="form-group">
												<input type="text" name="telephone" className="form-control" placeholder="Your Telephone"/>
											</div>
										</div>
									</div>
								
									<br/>
									<div className="row">
										<div className="col-md-12">
											<div className="form-group terms">
												<input name="terms" type="checkbox" className="icheck required" value="yes"/>
												<label>Please accept <a href="#" data-toggle="modal" data-target="#terms-txt">terms and conditions</a> ?</label>
											</div>
										</div>
									</div>
								</div>
								
							</div>
							
							<div id="bottom-wizard">
								<button type="button" name="backward" className="backward">Backward </button>
								<button type="button" name="forward" className="forward">Forward</button>
								<button type="submit" name="process" className="submit">Submit</button>
							</div>
						
						</form>
					</div>
					
				</div>
			</div>
		</div>
	</main>

<Footer/>
</Layout>
	)




}
export default  review_version;