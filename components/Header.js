import React from 'react';

const Header = () => {

return (

    <section >
  	
	<div id="preloader">
		<div data-loader="circle-side"></div>
	</div>
    {/* <!-- /Preload --> */}
	
	<div id="loader_form">
		<div data-loader="circle-side-2"></div>
	</div>
    {/* <!-- /loader_form --> */}

	<header>
		<div className="container-fluid">
		    <div className="row">
                <div className="col-3">
                    <div id="logo_home">
                        <h1><a href="index.html">MAVIA | Register, Reservation, Questionare, Reviews form wizard</a></h1>
                    </div>
                </div>
                <div className="col-9">
                    <div id="social">
                        <ul>
                            <li><a href="#0"><i className="icon-facebook"></i></a></li>
                            <li><a href="#0"><i className="icon-twitter"></i></a></li>
                            <li><a href="#0"><i className="icon-google"></i></a></li>
                            <li><a href="#0"><i className="icon-linkedin"></i></a></li>
                        </ul>
                    </div>
                    {/* <!-- /social --> */}
                    <nav>
                        <ul className="cd-primary-nav">
                            <li><a href="index.html" className="animated_link">Register Version</a></li>
                            <li><a href="reservation_version.html" className="animated_link">Reservation Version</a></li>
                            <li><a href="questionare_version.html" className="animated_link">Questionare Version</a></li>
                            <li><a href="review_version.html" className="animated_link">Review Version</a></li>
                            <li><a href="about.html" className="animated_link">About Us</a></li>
                            <li><a href="contacts.html" className="animated_link">Contact Us</a></li>
                        </ul>
                    </nav>
                </div>
            </div>
		</div>
		{/* <!-- container --> */}
	</header>
	{/* <!-- End Header --> */}


    </section>
    
)




}
export default  Header;