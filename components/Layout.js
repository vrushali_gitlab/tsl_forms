import React from 'react';
import Head from 'next/head';

const Layout = (props) => {

        return (
           <div>
                <Head>

    <meta charset="utf-8"/>
	<meta http-equiv="X-UA-Compatible" content="IE=edge"/>
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no"/>
	<meta name="description" content="MAVIA - Register, Reservation, Questionare, Reviews form wizard"/>
	<meta name="author" content="Ansonika"/>
	<title>MAVIA | Register, Reservation, Questionare, Reviews form wizard</title>


	<link rel="shortcut icon" href="/assets/img/favicon.ico" type="image/x-icon"/>
	<link rel="apple-touch-icon" type="/assets/image/x-icon" href="/assets/img/apple-touch-icon-57x57-precomposed.png"/>
	<link rel="apple-touch-icon" type="/assets/image/x-icon" sizes="72x72" href="/assets/img/apple-touch-icon-72x72-precomposed.png"/>
	<link rel="apple-touch-icon" type="/assets/image/x-icon" sizes="114x114" href="/assets/img/apple-touch-icon-114x114-precomposed.png"/>
	<link rel="apple-touch-icon" type="/assets/image/x-icon" sizes="144x144" href="/assets/img/apple-touch-icon-144x144-precomposed.png"/>

	
	<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:300,300i,400,400i,700,700i" rel="stylesheet"/>


	<link href="/assets/css/bootstrap.min.css" rel="stylesheet"/>
	<link href="/assets/css/style.css" rel="stylesheet"/>
	<link href="/assets/css/responsive.css" rel="stylesheet"/>
	<link href="/assets/css/menu.css" rel="stylesheet"/>
	<link href="/assets/css/animate.min.css" rel="stylesheet"/>
	<link href="/assets/css/icon_fonts/css/all_icons_min.css" rel="stylesheet"/>
	<link href="/assets/css/skins/square/grey.css" rel="stylesheet"/>

	
	<link href="/assets/css/custom.css" rel="stylesheet"/>

	<script src="/assets/js/modernizr.js"></script>

   
	<script src="/assets/js/jquery-3.2.1.min.js"></script>
	
	<script src="/assets/js/common_scripts_min.js"></script>
	
	<script src="/assets/js/registration_wizard_func.js"></script>
	
	<script src="/assets/js/velocity.min.js"></script>
	<script src="/assets/js/main.js"></script>
	
	<script src="/assets/js/functions.js"></script>

    </Head>


                <div className="page-wrapper">

                {props.children}

                </div>

</div>
)
}
export default Layout;