import React from 'react';

const Footer = () => {

return (

    <section >
  	


	
	
	<footer id="home" className="clearfix">
		<p>© 2020 Tulevik</p>
		<ul>
			<li><a href="#0" className="animated_link">Purchase this template</a></li>
			<li><a href="#0" className="animated_link">Terms and conditions</a></li>
			<li><a href="#0" className="animated_link">Contacts</a></li>
		</ul>
	</footer>
	{/* <!-- end footer--> */}
	
	<div className="cd-overlay-nav">
		<span></span>
	</div>
	{/* <!-- cd-overlay-nav --> */}

	<div className="cd-overlay-content">
		<span></span>
	</div>
	{/* <!-- cd-overlay-content --> */}

	<a href="#0" className="cd-nav-trigger">Menu<span className="cd-icon"></span></a>

	{/* <!-- Modal terms --> */}
	<div className="modal fade" id="terms-txt" tabindex="-1" role="dialog" aria-labelledby="termsLabel" aria-hidden="true">
		<div className="modal-dialog modal-dialog-centered">
			<div className="modal-content">
				<div className="modal-header">
					<h4 className="modal-title" id="termsLabel">Terms and conditions</h4>
					<button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div className="modal-body">
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
				</div>
				<div className="modal-footer">
					<button type="button" className="btn_1" data-dismiss="modal">Close</button>
				</div>
			</div>
			{/* <!-- /.modal-content --> */}
		</div>
		{/* <!-- /.modal-dialog --> */}
	</div>
	{/* <!-- /.modal --> */}

	{/* <!-- Modal info --> */}
	<div className="modal fade" id="more-info" tabindex="-1" role="dialog" aria-labelledby="more-infoLabel" aria-hidden="true">
		<div className="modal-dialog modal-dialog-centered">
			<div className="modal-content">
				<div className="modal-header">
					<h4 className="modal-title" id="more-infoLabel">Frequently asked questions</h4>
					<button type="button" className="close" data-dismiss="modal" aria-hidden="true">&times;</button>
				</div>
				<div className="modal-body">
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in <strong>nec quod novum accumsan</strong>, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus. Lorem ipsum dolor sit amet, <strong>in porro albucius qui</strong>, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
					<p>Lorem ipsum dolor sit amet, in porro albucius qui, in nec quod novum accumsan, mei ludus tamquam dolores id. No sit debitis meliore postulant, per ex prompta alterum sanctus, pro ne quod dicunt sensibus.</p>
				</div>
				<div className="modal-footer">
					<button type="button" className="btn_1" data-dismiss="modal">Close</button>
				</div>
			</div>
			{/* <!-- /.modal-content --> */}
		</div>
		{/* <!-- /.modal-dialog --> */}
	</div>
	{/* <!-- /.modal --></div> */}


</section>
    
)




}
export default  Footer;